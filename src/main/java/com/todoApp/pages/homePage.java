package com.todoApp.pages;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class homePage extends basePage {

	By signinLinkLocator = By.xpath("//a[contains(text(),'Sign In')]");
	By myTasksLocator = By.linkText("My Tasks");
	public static final String URL = "http://qa-test.avenuecode.com/";
	
	public homePage(WebDriver driver) {
		super(driver);
	    driver.navigate().to(URL);
	    maximizeWindow();
	    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	    assertEquals("ToDo Rails and Angular", driver.getTitle());
	 }
	
	public loginPage navigateLoginPage() {

		driver.findElement(signinLinkLocator).click();
		return new loginPage(driver);
	}
	public taskPage navigateTaskPage() {

		driver.findElement(myTasksLocator).click();
		return new taskPage(driver);
	}
	public void myTaskLinkPresent(){
		assertTrue("My Tasks link is present",isElementPresent(myTasksLocator));
	}
	public void clickMyTaskLink() throws InterruptedException{
		buttonClick(myTasksLocator);
	}
}