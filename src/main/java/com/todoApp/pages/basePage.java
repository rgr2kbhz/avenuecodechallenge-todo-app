package com.todoApp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class basePage {
	
	public static WebDriver driver;
	By logoutLocator = By.linkText("Sign out");

	public basePage(WebDriver driver) {
		this.driver = driver;
		maximizeWindow();
	}

	public boolean isElementPresent(By locator) {
		boolean isPresent = driver.findElements(locator).size() > 0;
		return isPresent;
	}
	
	public boolean isElementNotPresent(By locator) {
		boolean isNotPresent = driver.findElements(locator).size() < 0;
		return isNotPresent;
	}

    public void  loginCredentials(By Locator, String text)
    {
     driver.findElement(Locator).sendKeys(text);
    }
    
    public void buttonClick(By Locator)
    {
     driver.findElement(Locator).click();
    }
    
    public void maximizeWindow()
    {
    	driver.manage().window().maximize();
    }

    public void logout()
    {
    	buttonClick(logoutLocator);
    }
    
}