package com.todoApp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class loginPage extends basePage{
	
	//LoginPage Locators
	
	By signinLinkLocator = By.xpath("//a[contains(text(),'Sign In')]");
	By usernameLocator = By.id("user_email");
	By passwordLocator = By.id("user_password");
	By signinButtonLocator = By.xpath("//input[@value='Sign in']");
	By signedinMessageLocator = By.xpath("//div[contains(text(),'Signed in')]");
	By alertInfoLocator = By.cssSelector(".alert.alert-info");
	
	public loginPage(WebDriver driver) {
		super(driver);
}
	public loginPage loginToDoApp(){
		String Username = "rodrigo.guimaraes@gmail.com";
		String Password = "4v3nu3c0d31@3";
		buttonClick(signinLinkLocator);
		loginCredentials(usernameLocator,Username);
		loginCredentials(passwordLocator,Password);
		buttonClick(signinButtonLocator);
		return new loginPage(driver);
		
	}
	
	public void checkText(){
		Assert.assertEquals(driver.findElement(alertInfoLocator).getText(),"Signed in successfully.");
	}
}