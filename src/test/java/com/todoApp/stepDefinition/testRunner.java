package com.todoApp.stepDefinition;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="features",
plugin = {"html:target/cucumber-html-report", "json:target/cucumber-json-report.json" }
)

public class testRunner {
}