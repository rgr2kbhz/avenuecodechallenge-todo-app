# README #

This README would normally document whatever steps are necessary to get the application up and running.

### What is this repository for? ###

This is a Java project for the exam QA Automated Engineer at Avenue Code.

### How do I get set up? ###

This project was developed in **Java** using the **Selenium Webdriver**, **Cucumber** and **Maven** for dependency management.

**Dependencies:** Maven is required to run the tests. All the dependencies are listed in the **pom.xml file** and it is automatically downloaded from the Maven repository at the building process.

**Reports**: The Cucumber reports can be found in: '/target/cucumber-html-report/index.html'

**How to run the tests:** at the project **root** avenueCodeChallenge-TODO-APP/, execute from the command line the command "**mvn clean**" to be sure no data from past tests are stored and then "**mvn test**" in order to  run the tests.

### STLC Artifacts ###

* The PDF file (ACTest-Considerations.pdf) that has all my considerations is at the root of the project.

* User and Password to view the bugs registered at http://qa-test.avenuecode.com/bugs

  **user:** "rodrigo.guimaraes@gmail.com";
  **password:** "4v3nu3c0d31@3";