Feature: Create Task
  
  As a ToDo App user
  I should be able to create a task
  So I can manage my tasks

Scenario: Check for My Tasks link on navigation bar
Given I am logged into ToDo Application
Then I see loging in sucessfull
And I see My Tasks link on Navigation Bar

Scenario: Click on My Tasks link on navigation bar
Given I am logged into ToDo Application
Then I see loging in sucessfull
When I see My Tasks link on NavBar
Then I click on My Tasks link on NavBar

Scenario: Add new task
Given I am logged into ToDo Application
Then I see loging in sucessfull
When I see My Tasks link on NavBar
Then I click on My Tasks link on NavBar
And enter new task

Scenario: Should not be able to create new task with less than 3 characters
Given I am logged into ToDo Application
Then I see loging in sucessfull
When I see My Tasks link on NavBar
Then I click on My Tasks link on NavBar
And enter new task with less than 3 characters