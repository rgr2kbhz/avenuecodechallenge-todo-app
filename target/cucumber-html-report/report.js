$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("US#1_createTask.feature");
formatter.feature({
  "line": 1,
  "name": "Create Task",
  "description": "\nAs a ToDo App user\nI should be able to create a task\nSo I can manage my tasks",
  "id": "create-task",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2728490282,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Check for My Tasks link on navigation bar",
  "description": "",
  "id": "create-task;check-for-my-tasks-link-on-navigation-bar",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "I am logged into ToDo Application",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I see loging in sucessfull",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I see My Tasks link on Navigation Bar",
  "keyword": "And "
});
formatter.match({
  "location": "toDoAppTest.I_am_logged_into_ToDo_Application()"
});
formatter.result({
  "duration": 16740315886,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_loging_in_sucessfull()"
});
formatter.result({
  "duration": 27200788,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_My_Tasks_link_on_Navigation_Bar()"
});
formatter.result({
  "duration": 39866993,
  "status": "passed"
});
formatter.after({
  "duration": 82581032,
  "status": "passed"
});
formatter.before({
  "duration": 1986826051,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Click on My Tasks link on navigation bar",
  "description": "",
  "id": "create-task;click-on-my-tasks-link-on-navigation-bar",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I am logged into ToDo Application",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I see loging in sucessfull",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "I see My Tasks link on NavBar",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I click on My Tasks link on NavBar",
  "keyword": "Then "
});
formatter.match({
  "location": "toDoAppTest.I_am_logged_into_ToDo_Application()"
});
formatter.result({
  "duration": 18701764720,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_loging_in_sucessfull()"
});
formatter.result({
  "duration": 28762436,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 38011598,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_click_on_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 6283716985,
  "status": "passed"
});
formatter.after({
  "duration": 79535086,
  "status": "passed"
});
formatter.before({
  "duration": 1995783275,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Add new task",
  "description": "",
  "id": "create-task;add-new-task",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 19,
  "name": "I am logged into ToDo Application",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "I see loging in sucessfull",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "I see My Tasks link on NavBar",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "I click on My Tasks link on NavBar",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "enter new task",
  "keyword": "And "
});
formatter.match({
  "location": "toDoAppTest.I_am_logged_into_ToDo_Application()"
});
formatter.result({
  "duration": 23618001880,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_loging_in_sucessfull()"
});
formatter.result({
  "duration": 29879566,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 35507544,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_click_on_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 5797627075,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.enter_new_task()"
});
formatter.result({
  "duration": 156956851,
  "status": "passed"
});
formatter.after({
  "duration": 76931863,
  "status": "passed"
});
formatter.before({
  "duration": 1995386738,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Should not be able to create new task with less than 3 characters",
  "description": "",
  "id": "create-task;should-not-be-able-to-create-new-task-with-less-than-3-characters",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 26,
  "name": "I am logged into ToDo Application",
  "keyword": "Given "
});
formatter.step({
  "line": 27,
  "name": "I see loging in sucessfull",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "I see My Tasks link on NavBar",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "I click on My Tasks link on NavBar",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "enter new task with less than 3 characters",
  "keyword": "And "
});
formatter.match({
  "location": "toDoAppTest.I_am_logged_into_ToDo_Application()"
});
formatter.result({
  "duration": 22779112164,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_loging_in_sucessfull()"
});
formatter.result({
  "duration": 23533476,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 35289567,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_click_on_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 5807891730,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.enter_new_task_less_than_3_characters()"
});
formatter.result({
  "duration": 154444766,
  "status": "passed"
});
formatter.after({
  "duration": 75511551,
  "status": "passed"
});
formatter.uri("US#2_createSubTask.feature");
formatter.feature({
  "line": 1,
  "name": "Create SubTask",
  "description": "\nAs a ToDo App user\nI should be able to create a subtask\nSo I can break down my tasks in smaller pieces",
  "id": "create-subtask",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2028156441,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Check for Manage SubTask button",
  "description": "",
  "id": "create-subtask;check-for-manage-subtask-button",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "I am logged into ToDo Application",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I see loging in sucessfull",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I see My Tasks link on NavBar",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I click on My Tasks link on NavBar",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I am on ToDo MyTask Page",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I see Manage SubTask button",
  "keyword": "Then "
});
formatter.match({
  "location": "toDoAppTest.I_am_logged_into_ToDo_Application()"
});
formatter.result({
  "duration": 14640206171,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_loging_in_sucessfull()"
});
formatter.result({
  "duration": 25476110,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 45977993,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_click_on_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 5567871683,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_am_on_ToDo_MyTask_Page()"
});
formatter.result({
  "duration": 6433198,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_Manage_SubTask_button()"
});
formatter.result({
  "duration": 360489699,
  "status": "passed"
});
formatter.after({
  "duration": 81594810,
  "status": "passed"
});
formatter.before({
  "duration": 1967540871,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Click on Manage SubTask button",
  "description": "",
  "id": "create-subtask;click-on-manage-subtask-button",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 16,
  "name": "I am logged into ToDo Application",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "I see loging in sucessfull",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I see My Tasks link on NavBar",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I click on My Tasks link on NavBar",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "I am on ToDo MyTask Page",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "I see Manage SubTask button",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "I click on Manage SubTask button",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I see subTask pop page",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "I close sub task popup",
  "keyword": "Then "
});
formatter.match({
  "location": "toDoAppTest.I_am_logged_into_ToDo_Application()"
});
formatter.result({
  "duration": 17010562864,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_loging_in_sucessfull()"
});
formatter.result({
  "duration": 23660495,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 33671437,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_click_on_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 5735857934,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_am_on_ToDo_MyTask_Page()"
});
formatter.result({
  "duration": 8221111,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_Manage_SubTask_button()"
});
formatter.result({
  "duration": 459634619,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_click_on_Manage_SubTask_button()"
});
formatter.result({
  "duration": 205465166,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_subTask_pop_page()"
});
formatter.result({
  "duration": 14798551,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_close_sub_task_popup()"
});
formatter.result({
  "duration": 124377891,
  "status": "passed"
});
formatter.after({
  "duration": 83075881,
  "status": "passed"
});
formatter.before({
  "duration": 2028769790,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "Add new SubTask",
  "description": "",
  "id": "create-subtask;add-new-subtask",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 27,
  "name": "I am logged into ToDo Application",
  "keyword": "Given "
});
formatter.step({
  "line": 28,
  "name": "I see loging in sucessfull",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "I see My Tasks link on NavBar",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "I click on My Tasks link on NavBar",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "I am on ToDo MyTask Page",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "I see Manage SubTask button",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "I click on Manage SubTask button",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "I see subTask pop page",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "add new sub task",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "I close sub task popup",
  "keyword": "And "
});
formatter.match({
  "location": "toDoAppTest.I_am_logged_into_ToDo_Application()"
});
formatter.result({
  "duration": 13606295208,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_loging_in_sucessfull()"
});
formatter.result({
  "duration": 24813677,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 32132258,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_click_on_My_Tasks_link_on_NavBar()"
});
formatter.result({
  "duration": 5571186805,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_am_on_ToDo_MyTask_Page()"
});
formatter.result({
  "duration": 7743108,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_Manage_SubTask_button()"
});
formatter.result({
  "duration": 520548702,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_click_on_Manage_SubTask_button()"
});
formatter.result({
  "duration": 208069376,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_see_subTask_pop_page()"
});
formatter.result({
  "duration": 26836846,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.add_new_sub_task()"
});
formatter.result({
  "duration": 444920109,
  "status": "passed"
});
formatter.match({
  "location": "toDoAppTest.I_close_sub_task_popup()"
});
formatter.result({
  "duration": 167441072,
  "status": "passed"
});
formatter.after({
  "duration": 88770548,
  "status": "passed"
});
});